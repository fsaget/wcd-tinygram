# WCD TinyGram

A Java-based, Google Cloud-powered Instagram clone produced for the *Web, Cloud and Datastores* 2021 projet.

## Team members

- [Thomas Georges](https://gitlab.com/ThomGeor) (E171996C) : Master 1 ALMA
- [Léo Olivier](https://gitlab.com/ekkaia) (E187954Y) : Master 1 ALMA
- [Félix Saget](https://gitlab.com/fsaget) (E187076T) : Master 1 VICO

## Before building

Make sure you have installed the [Google Cloud SDK](https://cloud.google.com/sdk/docs/install) as well as the AppEngine Java components :
```
gcloud components install app-engine-java
```

Setup an AppEngine project on the [Google Cloud Platform](https://console.cloud.google.com/home/dashboard) and generate an OAuth 2.0 Client ID.

- in [Config.java](src/main/java/utils/Config.java), replace the `clientIds` & `audiences` values by your own OAuth 2.0 Client ID ;
- in [appengine-web.xml](src/main/webapp/WEB-INF/appengine-web.xml), replace the `endpoints` value with your AppEngine project's target URL (usually *project-name*.oa.r.appspot.com) ;
- in [pom.xml](pom.xml), replace the Endpoints Project ID by your own project's ID.

## Build & run

### Local
```
mvn install 
mvn clean package 
mvn appengine:run
```

*Note :* the Google API will not work as intended when the project is launched locally. To retrieve all of the user's information, it is necessary to deploy (see below). 

### Remote with the Google Cloud SDK
```
gcloud auth login
gcloud config set project <your_remote_project_name>
mvn install
mvn clean package
mvn appengine:deploy
```

*Note :* if you do not wish to login with your Google account on the Google Cloud SDK, you can use the following command to authenticate with a service account generated on the Google Cloud Platform :
```
gcloud auth activate-service-account <your-remote-project-name>@appspot.gserviceaccount.com \
    --key-file=<path-to-json-key> \
    --project=<your-remote-project-name>
```

## Implementation details

## Useful links

- Endpoint Portal : https://endpointsportal.tiny-gram-wcd.cloud.goog/docs/tiny-gram-wcd.appspot.com/1/introduction
- Frontend GitLab repository : https://gitlab.com/ekkaia/wcd-endpoints-front

### Kinds

- User
    - `id` : Google API UID
    - `date` : last login
    - `email` : email (from the Google API)
    - `image_url` : profile picture (from the Google API)
    - `name` : full name (from the Google API)
- Post
    - `id` : auto-generated post UID
    - `Date` : date of posting
    - `Description` : post description
    - `Likes` : number of likes
    - `Url` : image URL
    - `User` : UID of poster
- Followers
    - `id` : auto-generated follow UID
    - `User` : the user following
    - `Follow` : the user being followed
- FollowersNumber
    - `id` : auto-generated counter UID
    - `User` : the user 
    - `Total` : total number of followers
- Like counter (counter-{postId})
    - `name` : 1::10
    - `counterBucketPost` : number of likes in the selected bucket

### Performances

*average benchmarks on ~30 measures*
- time to post a message is not affected by followers count ;
- time to retrieve latest posts (feed) :
    - ...of size 10 : ~0.29s
    - ...of size 100 : ~0.37s
    - ...of size 500 : ~0.98s
- average number of likes per second : ~5629

## Try it out!

At [endpoints.netlify.app](https://tinygram.netlify.app).

We have registered our Google accounts on Tinygram, so you can find our posts and follow us! Try to search for "brochard", "True Duke", "Thomas Georges". Don't pay too much attention to the content of the posts 😄
