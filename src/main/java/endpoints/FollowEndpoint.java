package endpoints;

import com.google.api.server.spi.auth.common.User;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.Named;
import com.google.appengine.api.datastore.*;

import java.util.List;

import static utils.Config.AUDIENCES_ID;
import static utils.Config.CLIENT_ID;

/**
 * Follow controller
 */
@Api(name = "api", version = "v1", clientIds = CLIENT_ID, audiences = AUDIENCES_ID)
public class FollowEndpoint {
	/**
	 * User follow count
	 * @param userId user to retrieve his follow count
	 * @return Follow entity containing a user follow count
	 */
	@ApiMethod(name = "findFollowCount", path = "members/followers_count", httpMethod = ApiMethod.HttpMethod.GET)
	public Entity findFollowCount(@Named("userId") String userId) {
		int numberOfFollow;

		Query queryFollow = new Query("Followers")
				.setFilter(new Query.FilterPredicate("User", Query.FilterOperator.EQUAL, userId));
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

		List<Entity> results = datastore.prepare(queryFollow).asList(FetchOptions.Builder.withDefaults());
		numberOfFollow = results.size();

		Entity entityNumFollow = new Entity("FollowersNumber");
		entityNumFollow.setProperty("User", userId);
		entityNumFollow.setProperty("Total", numberOfFollow);

		Transaction txn = datastore.beginTransaction();
		datastore.put(txn, entityNumFollow);
		txn.commit();
		return entityNumFollow;
	}

	/**
	 * Retrieve followed users
	 * @param userId user to retrieve his follows
	 * @return Entity list of follows containing followed users
	 */
	@ApiMethod(name = "findFollowedUsers", path = "members/follows", httpMethod = ApiMethod.HttpMethod.GET)
	public static List<Entity> findFollowedUsers(@Named("userId") String userId) {
		Query queryFollow = new Query("Followers")
				.setFilter(new Query.FilterPredicate("User", Query.FilterOperator.EQUAL, userId));
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		return datastore.prepare(queryFollow).asList(FetchOptions.Builder.withDefaults());
	}

	/**
	 * Follow another user
 	 * @param user current auth user
	 * @param followUserId followed user id
	 * @return Follow entity created
	 */
	@ApiMethod(name = "createFollow", path = "members/follow", httpMethod = ApiMethod.HttpMethod.POST)
	public Entity createFollow(User user, @Named("followUserId") String followUserId) {
		Query queryFollow = new Query("Followers")
				.setFilter(new Query.FilterPredicate("User", Query.FilterOperator.EQUAL, user.getId()))
				.setFilter(new Query.FilterPredicate("Follow", Query.FilterOperator.EQUAL, followUserId));

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		List<Entity> listFollow = datastore.prepare(queryFollow).asList(FetchOptions.Builder.withDefaults());
		int size = listFollow.size();

		if (size != 0) {
			return listFollow.get(0);
		}

		Entity followEntity = new Entity("Followers");
		followEntity.setProperty("User", user.getId());
		followEntity.setProperty("Follow", followUserId);

		Transaction txn = datastore.beginTransaction();
		datastore.put(followEntity);
		txn.commit();

		return followEntity;
	}

	/**
	 * Unfollow a user
	 * @param user current auth user
	 * @param followedUserId followed user id
	 */
	@ApiMethod(name = "removeFollow", path = "members/follow", httpMethod = ApiMethod.HttpMethod.DELETE)
	public void removeFollow(User user, @Named("followedUserId") String followedUserId) {
		Query queryFollow = new Query("Followers")
				.setFilter(new Query.FilterPredicate("User", Query.FilterOperator.EQUAL, followedUserId))
				.setFilter(new Query.FilterPredicate("Follow", Query.FilterOperator.EQUAL, user.getId()));

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		List<Entity> listFollow = datastore.prepare(queryFollow).asList(FetchOptions.Builder.withLimit(1));
		Entity entity = listFollow.get(0);

		Transaction txn = datastore.beginTransaction();
		datastore.delete(txn, entity.getKey());
		txn.commit();
	}
}
