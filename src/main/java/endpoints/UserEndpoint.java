package endpoints;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.Named;
import com.google.appengine.api.datastore.*;
import com.google.appengine.api.users.User;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static utils.Config.AUDIENCES_ID;
import static utils.Config.CLIENT_ID;

/**
 * User controller
 */
@Api(name = "api", version = "v1", clientIds = CLIENT_ID, audiences = AUDIENCES_ID)
public class UserEndpoint {
	/**
	 * Handle login & authentication
	 * @param user user trying to authenticate
	 * @param image_url url of the user's profile picture
	 * @return created user entity
	 */
	@ApiMethod(name = "userLogin", path = "auth/login", httpMethod = ApiMethod.HttpMethod.POST)
	public Entity userLogin(User user, @Named("image_url") String image_url) {
		Entity entity = new Entity("User", user.getUserId());
		entity.setProperty("date", new Date());
		entity.setProperty("email", user.getEmail());
		entity.setProperty("name", user.getNickname());
		entity.setProperty("image_url", image_url);

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Transaction transaction = datastore.beginTransaction();
		datastore.put(transaction, entity);
		transaction.commit();

		return entity;
	}

	/**
	 * Search for users by exact name
	 * @param name username to query
	 * @return user entity found
	 */
	@ApiMethod(name = "findUserByName", path = "members/search", httpMethod = ApiMethod.HttpMethod.GET)
	public Entity findOneUserByName(@Named("name") String name){
		Query queryFollow = new Query("User")
				.setFilter(new Query.FilterPredicate("name", Query.FilterOperator.EQUAL, name));
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		List<Entity> listUser = datastore.prepare(queryFollow).asList(FetchOptions.Builder.withLimit(1));
		return listUser.get(0);
	}

	/**
	 * Search for users by partial name
	 * @param prefix string to match with user in Datastore
	 * @return entity list of users found
	 */
	@ApiMethod(name = "findAllUsersByPrefix", path = "members/prefix", httpMethod = ApiMethod.HttpMethod.GET)
	public List<Entity> findAllUsersByPrefix(@Named("prefix") String prefix) {
		Query users = new Query("User")
				.setFilter(new Query.FilterPredicate("name", Query.FilterOperator.GREATER_THAN_OR_EQUAL, prefix))
				.setFilter(new Query.FilterPredicate("name", Query.FilterOperator.LESS_THAN, prefix + "\ufffd"));

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		return datastore.prepare(users).asList(FetchOptions.Builder.withLimit(20));
	}

	/**
	 * Retrieve user profile
	 * @param userId user id of the requested user profile
	 * @return user profile entity found
	 * @throws EntityNotFoundException No user found
	 */
	@ApiMethod(name = "findUserProfile", path = "members/profile", httpMethod = ApiMethod.HttpMethod.GET)
	public Entity findUserProfile(@Named("user") String userId) throws EntityNotFoundException {
		Key keyOfUser = KeyFactory.createKey("User", userId);
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		return datastore.get(keyOfUser);
	}

	/**
	 * Get user list
	 * @return entity list of users in the Datastore
	 */
	@ApiMethod(name = "findAllUsers", path = "members/browse", httpMethod = ApiMethod.HttpMethod.GET)
	public List<Entity> findAllUsers() {
		Query query = new Query("User");
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		return datastore.prepare(query).asList(FetchOptions.Builder.withDefaults());
	}

	/**
	 * Delete user from Datastore
	 * @param userId user to delete
	 */
	@ApiMethod(name = "removeUser", path = "members/profile", httpMethod = ApiMethod.HttpMethod.DELETE)
	public void removeUser(@Named("userId") String userId) {
		Key keyOfUser = KeyFactory.createKey("User", userId);
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Transaction txn = datastore.beginTransaction();
		datastore.delete(txn, keyOfUser);
		txn.commit();
	}

	/**
	 * Recent posts list from followed users
	 * @return entity list of post from followed users of a given user
	 */
	@ApiMethod(name = "findUserFeed", path = "posts/feed", httpMethod = ApiMethod.HttpMethod.GET)
	public List<Entity> findUserFeed(User user) {
		List<Entity> follows = FollowEndpoint.findFollowedUsers(user.getUserId());
		List<Entity> n = new ArrayList<>();

		for (Entity e : follows) {
			Query getPosts = new Query("Post")
					.setFilter(new Query.FilterPredicate("User", Query.FilterOperator.EQUAL, e.getProperty("Follow")));

			List<Entity> result = DatastoreServiceFactory.getDatastoreService()
					.prepare(getPosts)
					.asList(FetchOptions.Builder.withDefaults());

			n.addAll(result);
		}
		n.sort(Comparator.comparing(o -> ((Date) o.getProperty("Date"))));
		return n;
	}
}
