package endpoints;

import com.google.api.server.spi.auth.common.User;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.Named;
import com.google.appengine.api.datastore.*;
import entities.Post;

import java.util.Date;
import java.util.List;

import static utils.Config.AUDIENCES_ID;
import static utils.Config.CLIENT_ID;

/**
 * Post controller
 */
@Api(name = "api", version = "v1", clientIds = CLIENT_ID, audiences = AUDIENCES_ID)
public class PostEndpoint {
	/**
	 * Send a post to the Datastore
	 * @param imageUrl Post picture url
	 * @param description Post description
	 * @return Post entity created
	 */
	@ApiMethod(name = "createPost", path = "posts", httpMethod = ApiMethod.HttpMethod.POST)
	public Entity createPost(
			User user,
			@Named("imageUrl") String imageUrl,
			@Named("description") String description
	) {
		Entity postEntity = new Entity("Post");
		postEntity.setProperty("Url", imageUrl);
		postEntity.setProperty("User", user.getId());
		postEntity.setProperty("Description", description);
		postEntity.setProperty("Date", new Date());
		postEntity.setProperty("Likes", 0);

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Transaction transaction = datastore.beginTransaction();
		datastore.put(transaction, postEntity);
		transaction.commit();

		return postEntity;
	}

	/**
	 * Delete a post
	 * @param postId id from post to delete
	 */
	@ApiMethod(name = "removePost", path = "posts", httpMethod = ApiMethod.HttpMethod.DELETE)
	public void removePost(@Named("postId") String postId) {
		Key keyOfUser = KeyFactory.createKey("Post", Long.parseLong(postId));
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Transaction transaction = datastore.beginTransaction();
		datastore.delete(transaction, keyOfUser);
		transaction.commit();
	}

	/**
	 * Get user's post
	 * @return Entity list of posts
	 */
	@ApiMethod(name = "findAllPostsByUser", path = "members/posts", httpMethod = ApiMethod.HttpMethod.GET)
	public List<Entity> findAllPostsByUser(User user) {
		Query queryFollow = new Query("Post").setFilter(new Query.FilterPredicate("User", Query.FilterOperator.EQUAL, user.getId()));
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		return datastore.prepare(queryFollow).asList(FetchOptions.Builder.withDefaults());
	}

	/**
	 * Retrieve a post with its id
	 * @param postId if from post to retrieve
	 * @return Post entity found
	 * @throws EntityNotFoundException Post not found
	 */
	@ApiMethod(name = "findOnePost", path = "posts", httpMethod = ApiMethod.HttpMethod.GET)
	public Entity findOnePost(@Named("postId") String postId) throws EntityNotFoundException {
		Key keyOfPost = KeyFactory.createKey("Post", Long.parseLong(postId));
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		return datastore.get(keyOfPost);
	}

	/**
	 * Like a post
	 * @param postId post to like
	 * @return Post entity updated
	 * @throws EntityNotFoundException Post not found
	 */
	@ApiMethod(name = "likePost", path = "posts/like", httpMethod = ApiMethod.HttpMethod.PUT)
	public Entity likePost(@Named("postId") String postId) throws EntityNotFoundException{
		Key keyOfUser = KeyFactory.createKey("Post", Long.parseLong(postId));

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Transaction transaction = datastore.beginTransaction();
		Entity entity = datastore.get(transaction, keyOfUser);
		transaction.commit();

		Post newPost = new Post(postId);
		newPost.addLikePost();
		entity.setProperty("Likes", newPost.getNumberOfLikes());

		Transaction txn = datastore.beginTransaction();
		datastore.put(entity);
		txn.commit();

		return entity;
	}
}
