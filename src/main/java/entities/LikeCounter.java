package entities;

import com.google.appengine.api.datastore.*;

import java.util.Random;

/**
 * Like counter for a post
 */
public class LikeCounter {
    private final String kind;
    private final int numberOfKind;
    private final String type;

    public LikeCounter(String id, String type) {
        this.kind = "counter-" + id;
        this.numberOfKind = 10;
        this.type = type;
    }

    /**
     * Add a like to a given post
     */
    public void addLike() {
        Random rand = new Random();
        long numberPartition = rand.nextInt(this.numberOfKind);
        Key key = KeyFactory.createKey(kind, Long.toString(numberPartition));

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Transaction txn = datastore.beginTransaction();

        try {
            Entity entity = datastore.get(key);
            long counter = (Long) entity.getProperty("counterBucket"+this.type) + 1;
            entity.setProperty("counterBucket"+this.type, counter);
            datastore.put(entity);
            txn.commit();
        } catch (EntityNotFoundException e) {
            Entity entity = new Entity(key);
            long likes = 1;
            entity.setProperty("counterBucket"+this.type, likes);
            datastore.put(entity);
            txn.commit();
        }

    }

    /**
     * Retrieve like
     * @return Long representing like count
     */
    public Long getLikesCount() {
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        long resultLikes = 0;
        Query queryOfLikes = new Query(this.kind);

        for (Entity differentKindValue : datastore.prepare(queryOfLikes).asIterable()) {
            resultLikes += (Long) differentKindValue.getProperty("counterBucket" + this.type);
        }

        return resultLikes;
    }
}
