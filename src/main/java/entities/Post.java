package entities;

public class Post {
    private final LikeCounter likes;

    public Post(String postId) {
        this.likes = new LikeCounter(postId, "Post");
    }

    /**
     * Call for adding a like to a post
     */
    public void addLikePost(){
        likes.addLike();
    }

    /**
     * Call for getting post like count
     * @return like count
     */
    public long getNumberOfLikes() {
        return likes.getLikesCount();
    }
}
